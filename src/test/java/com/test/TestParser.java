package com.test;

import java.io.File;

import com.shareyi.wox.config.WoxConfig;
import com.shareyi.wox.config.WoxConfigException;
import com.shareyi.wox.core.WoxConfiguartion;
import com.shareyi.wox.util.WoxConfigParseUtils;
import com.shareyi.fileutil.FileUtil;

public class TestParser {

	public static void main(String[] args) throws WoxConfigException {
		 WoxConfigParseUtils configParseUtils=new WoxConfigParseUtils();
		 File file=new File(FileUtil.getRuntimeFilePath("/config/wox.xml"));
		 WoxConfig woxConfig=configParseUtils.parseWoxConfig(file);
		 System.out.println(woxConfig.getPackageBeans().get(0).getGlobalResultBeans().get(0).getPath());
		 
		 WoxConfiguartion woxConfiguartion=new WoxConfiguartion(woxConfig, null);
		 woxConfiguartion.parseWoxConfig(woxConfig, null);
	}
}
