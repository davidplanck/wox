package com.test;

import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.shareyi.wox.util.WildcardHelper;

public class TestPattern {

	public static void main(String[] args) throws URISyntaxException {
        WildcardHelper w = new WildcardHelper();
        int[] a = w.compilePattern("*d*_*");
        for (int i = 0; i < a.length; i++) {
            int j = a[i];
            System.out.print(j + ",");
        }
        Map<String,String> vars = new LinkedHashMap<String,String>();
        //User_add.action,页面请求的action，请求通过struts2的处理后，他会把后缀action给截取掉
        //所以，我们模拟的时候，将action给去掉
        w.match(vars, "mhdsdsss", a);
        System.out.println();
        System.out.println(vars);
    }
}
