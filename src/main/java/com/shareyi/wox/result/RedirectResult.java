package com.shareyi.wox.result;

import com.shareyi.wox.context.ServletActionContext;
import com.shareyi.wox.core.ActionInvocation;

public class RedirectResult extends WoxResultSupport {

	private static final long serialVersionUID = -4945844436073695601L;

	@Override
	protected void doExecute(String finalLocation, ActionInvocation invocation)
			throws Exception {
		
			ServletActionContext.getResponse().redirect(finalLocation);
	}

}
