package com.shareyi.wox.result;

import com.shareyi.wox.context.ServletActionContext;
import com.shareyi.wox.core.ActionInvocation;
import com.shareyi.simpleserver.core.HttpRequest;
import com.shareyi.simpleserver.core.HttpResponse;
import com.shareyi.simpleserver.core.RequestDispatcher;

public class DispatcherResult extends WoxResultSupport{

	@Override
	protected void doExecute(String finalLocation, ActionInvocation invocation)
			throws Exception {
			HttpRequest request=ServletActionContext.getRequest();
			HttpResponse response=ServletActionContext.getResponse();
			RequestDispatcher dispatcher=request.getRequestDispacher(finalLocation);
			dispatcher.forward(request, response);
	}

	

}
