package com.shareyi.wox.result;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.shareyi.wox.config.ResultBean;
import com.shareyi.wox.core.ActionInvocation;
import com.shareyi.wox.core.Result;

public abstract class WoxResultSupport implements Result{

	 private static final Log LOG = LogFactory.getLog(WoxResultSupport.class);

	    /** The default parameter */
	    public static final String DEFAULT_PARAM = "location";

	    private boolean parse;
	    private boolean encode;
	    private String location;

	public boolean isParse() {
			return parse;
		}


		public void setParse(boolean parse) {
			this.parse = parse;
		}


		public boolean isEncode() {
			return encode;
		}


		public void setEncode(boolean encode) {
			this.encode = encode;
		}


		public String getLocation() {
			return location;
		}


		public void setLocation(String location) {
			this.location = location;
		}




	public void execute(ActionInvocation invocation) throws Exception {
		doExecute(location, invocation);
			
	}
	
	
	protected abstract void doExecute(String finalLocation, ActionInvocation invocation) throws Exception;
}
