package com.shareyi.wox.action;

public interface WoxAction {
	
	/**
	 * execute the action
	 * @return
	 * @throws Exception 
	 */
	public String execute() throws Exception;
	
}
