package com.shareyi.wox.action;

public abstract class WoxActionSupport implements WoxAction {

	   public static final String INPUT="input";
	   public static final String SUCCESS="success";
	   public static final String FAIL="fail";
	   public static final String ERROR="error";
	   public static final String NONE="none";
   	   public static final String FORWARD="forward";
   	   
   	   /** finish 将进入直接处理阶段  */
	   public static final String JSONBACK="jsonBack_wox";


	public  String execute() throws Exception {
		return SUCCESS;
	}
}
