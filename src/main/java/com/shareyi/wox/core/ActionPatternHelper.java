package com.shareyi.wox.core;

import java.util.Map;

import com.shareyi.wox.util.WildcardHelper;

public class ActionPatternHelper {
	
	private String expression;
	public int[] patternInfo;
	public boolean literal;
	
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public int[] getPatternInfo() {
		return patternInfo;
	}
	public void setPatternInfo(int[] patternInfo) {
		this.patternInfo = patternInfo;
	}
	
	
	public boolean isLiteral() {
		return literal;
	}
	public void setLiteral(boolean literal) {
		this.literal = literal;
	}
	public void parseActionName(String actionName,WildcardHelper helper){
		this.expression=actionName;
		this.literal=helper.isLiteral(expression);
		if(!literal){
			this.patternInfo=helper.compilePattern(expression);
		}
		
	}
	public boolean isResultMapMatch(Map<String, String> map) {
		int keySize=map.size()-1;
		int starCount=0;
		for(int i=0;i<patternInfo.length;i++){
			if(patternInfo[i]==-1){
				starCount++;
			}
		}
		return keySize==starCount;
	}
}
