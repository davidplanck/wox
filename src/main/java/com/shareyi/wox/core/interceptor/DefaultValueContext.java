package com.shareyi.wox.core.interceptor;

import java.util.HashMap;
import java.util.Map;

import com.shareyi.wox.context.ServletActionContext;
import com.shareyi.wox.context.ValueContext;

public class DefaultValueContext implements ValueContext {

	Map<String,Object> context=new HashMap<String, Object>();
	
	
	public Map<String, Object> getContext() {
		return context;
	}

	public void setValue(String expr, Object value) {
		context.put(expr, value);
	}

	public void setParameter(String expr, String value) {
		context.put(expr, value);
		ServletActionContext.getRequest().getParameterMap().put(expr, value);
	}

	public Object findValue(String expr) {
		return context.get(expr);
	}

	public int size() {
		return context.size();
	}

	public void putAll(Map map) {
		context.putAll(map);

	}

}
