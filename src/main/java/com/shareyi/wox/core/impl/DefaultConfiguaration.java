package com.shareyi.wox.core.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.shareyi.wox.config.ActionBean;
import com.shareyi.wox.config.InterceptorRef;
import com.shareyi.wox.config.PackageBean;
import com.shareyi.wox.core.ActionProxy;
import com.shareyi.wox.core.Configuration;

public class DefaultConfiguaration implements Configuration {

	
	private String namespace;
	private String actionName;
	private ActionBean actionBean;
	private PackageBean packageBean;
	private String methodName;
	private ActionProxy actionProxy;
	private Map<String,String> patternResult;
	
	
	
	
	
	
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public ActionBean getActionBean() {
		return actionBean;
	}
	public void setActionBean(ActionBean actionBean) {
		this.actionBean = actionBean;
	}
	public PackageBean getPackageBean() {
		return packageBean;
	}
	public void setPackageBean(PackageBean packageBean) {
		this.packageBean = packageBean;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public String getMethodName() {
		return this.methodName;
	}
	
	
	public boolean isAllowedMethod(String method) {
		return true;
	}
	public ActionProxy getActionProxy() {
		return this.actionProxy;
	}

	
	
	
	public List<InterceptorRef> getInterceptors() {
		List<InterceptorRef> interceptorRefs=actionBean.getInterceptorRefs();
		if(interceptorRefs==null){
			InterceptorRef defaultInterceptorRef=packageBean.getPackageDefaultInterceptorRef();
			if(defaultInterceptorRef!=null){
				interceptorRefs=new ArrayList<InterceptorRef>(1);
				interceptorRefs.add(defaultInterceptorRef);				
			}
		}
		
		if(interceptorRefs!=null){
			return packageBean.getInterceptorRefsWithoutStack(interceptorRefs);
		}
		return Collections.EMPTY_LIST;
	}
	public void setPatternResult(Map<String, String> patternResult) {
		this.patternResult=patternResult;
		
	}
	public Map<String, String> getPatternResult() {
		return patternResult;
	}
	public void setActionProxy(ActionProxy actionProxy) {
		this.actionProxy = actionProxy;
	}
	

	 
	
	
}
