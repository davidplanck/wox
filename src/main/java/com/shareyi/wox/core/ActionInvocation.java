package com.shareyi.wox.core;

import java.io.Serializable;

import com.shareyi.wox.config.ActionBean;
import com.shareyi.wox.config.ResultBean;
import com.shareyi.wox.result.WoxResultSupport;

public interface ActionInvocation  extends Serializable {

	    /**
	     * Gets whether this ActionInvocation has executed before.
	     * This will be set after the Action and the Result have executed.
	     *
	     * @return <tt>true</tt> if this ActionInvocation has executed before.
	     */
	    boolean isExecuted();


	    /**
	     * Get the ActionProxy holding this ActionInvocation.
	     *
	     * @return the ActionProxy.
	     */
	    ActionProxy getProxy();

	    /**
	     * If the ActionInvocation has been executed before and the Result is an instance of {@link ActionChainResult}, this method
	     * will walk down the chain of <code>ActionChainResult</code>s until it finds a non-chain result, which will be returned. If the
	     * ActionInvocation's result has not been executed before, the Result instance will be created and populated with
	     * the result params.
	     *
	     * @return the result.
	     * @throws Exception can be thrown.
	     */
	    WoxResultSupport getResult() throws Exception;

	    /**
	     * Gets the result code returned from this ActionInvocation.
	     *
	     * @return the result code
	     */
	    String getResultCode();

	    /**
	     * Sets the result code, possibly overriding the one returned by the
	     * action.
	     * <p/>
	     * The "intended" purpose of this method is to allow PreResultListeners to
	     * override the result code returned by the Action.
	     * <p/>
	     * If this method is used before the Action executes, the Action's returned
	     * result code will override what was set. However the Action could (if
	     * specifically coded to do so) inspect the ActionInvocation to see that
	     * someone "upstream" (e.g. an Interceptor) had suggested a value as the
	     * result, and it could therefore return the same value itself.
	     * <p/>
	     * If this method is called between the Action execution and the Result
	     * execution, then the value set here will override the result code the
	     * action had returned.  Creating an Interceptor that implements
	     * {@link PreResultListener} will give you this oportunity.
	     * <p/>
	     * If this method is called after the Result has been executed, it will
	     * have the effect of raising an IllegalStateException.
	     *
	     * @param resultCode  the result code.
	     * @throws IllegalStateException if called after the Result has been executed.
	     * @see #isExecuted()
	     */
	    void setResultCode(String resultCode);

	    /**
	     * Invokes the next step in processing this ActionInvocation.
	     * <p/>
	     * If there are more Interceptors, this will call the next one. If Interceptors choose not to short-circuit
	     * ActionInvocation processing and return their own return code, they will call invoke() to allow the next Interceptor
	     * to execute. If there are no more Interceptors to be applied, the Action is executed.
	     * If the {@link ActionProxy#getExecuteResult()} method returns <tt>true</tt>, the Result is also executed.
	     *
	     * @throws Exception can be thrown.
	     * @return the return code.
	     */
	    String invoke() throws Exception;

	    /**
	     * Invokes only the Action (not Interceptors or Results).
	     * <p/>
	     * This is useful in rare situations where advanced usage with the interceptor/action/result workflow is
	     * being manipulated for certain functionality.
	     *
	     * @return the return code.
	     * @throws Exception can be thrown.
	     */
	    String invokeActionOnly() throws Exception;



	    void init(WoxConfiguartion woxConfiguartion, Configuration configuration) throws Exception;

		public ActionBean getActionBean();


		/**
		 * 完成调用
		 */
		void finishInvoke() throws Exception;

	
}
