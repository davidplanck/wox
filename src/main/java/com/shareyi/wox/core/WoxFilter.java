package com.shareyi.wox.core;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.shareyi.wox.config.WoxConfig;
import com.shareyi.wox.config.WoxConfigException;
import com.shareyi.wox.context.ActionContext;
import com.shareyi.wox.context.ServletActionContext;
import com.shareyi.wox.context.ValueContext;
import com.shareyi.wox.core.impl.DefaultActionInvocation;
import com.shareyi.wox.core.impl.DefaultConfiguaration;
import com.shareyi.wox.core.interceptor.DefaultValueContext;
import com.shareyi.wox.result.WoxResultSupport;
import com.shareyi.wox.util.PackageAndActionResolver;
import com.shareyi.wox.util.WoxConfigParseUtils;
import com.shareyi.fileutil.FileUtil;
import com.shareyi.simpleserver.core.HttpRequest;
import com.shareyi.simpleserver.core.HttpResponse;
import com.shareyi.simpleserver.servlet.Filter;
import com.shareyi.simpleserver.servlet.FilterChain;
import com.shareyi.simpleserver.servlet.FilterConfig;
import com.shareyi.simpleserver.servlet.ServletException;

public class WoxFilter extends Filter {

	private boolean isOk=false;
	public static WoxConfiguartion woxConfiguartion;


	@Override
	public void destroy() {
	
		
	}

	@Override
	public void doFilter(HttpRequest request, HttpResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		
		if(!isOk)
		{
			response.setStatusCode(HttpURLConnection.HTTP_SERVER_ERROR);
			response.sendError(HttpURLConnection.HTTP_SERVER_ERROR, "Wox框架没有初始化");
			try {
				response.commitResponse();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	
	else
	{
		// 将本次访问的request,response,servletContext装入WoxAction的本地线程变量中....
		ValueContext valueContext=new DefaultValueContext();
		valueContext.putAll(request.getParameterMap());
		
		ServletActionContext.setActionContext(new ActionContext(request, response,valueContext));
		try {
			actionExecute(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatusCode(HttpURLConnection.HTTP_SERVER_ERROR);
			response.sendError(HttpURLConnection.HTTP_SERVER_ERROR, e.toString());
			try {
				response.commitResponse();
			} catch (IOException e2) {

			}
		}
		

	}
}


	private void actionExecute(HttpRequest request, HttpResponse response) throws Exception {
			DefaultConfiguaration configuaration=PackageAndActionResolver.getConfiguration(woxConfiguartion, request);
			PackageAndActionResolver.findPackageAndAction(woxConfiguartion, configuaration);
			ActionInvocation actionInvocation=new DefaultActionInvocation();
			actionInvocation.init(woxConfiguartion,configuaration);
			String resultCode=actionInvocation.invoke();
			if(StringUtils.isNotEmpty(resultCode)){  //如果返回为空，则不继续执行
				actionInvocation.finishInvoke();
				WoxResultSupport result=actionInvocation.getResult();
				result.execute(actionInvocation);	
			}
	}
	


	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String configPath=filterConfig.getInitParameter("configPath");
		WoxConfigParseUtils woxConfigParseUtils=new WoxConfigParseUtils();
		if(configPath==null){
			configPath="/config/wox-config.xml";
		}
		File file=new File(FileUtil.contactPath(FileUtil.getRunPath(),configPath));
		try {
			WoxConfig woxConfig=woxConfigParseUtils.parseWoxConfig(file);
			WoxFilter.woxConfiguartion=new WoxConfiguartion(woxConfig,filterConfig.getApplicationContext());
			isOk=true;
		} catch (WoxConfigException e) {
			throw new ServletException("初始化wox框架出错！",e);
		}
		
	}

}
