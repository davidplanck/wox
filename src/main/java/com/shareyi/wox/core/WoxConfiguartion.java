package com.shareyi.wox.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.shareyi.wox.config.PackageBean;
import com.shareyi.wox.config.WoxConfig;
import com.shareyi.wox.config.WoxConfigException;
import com.shareyi.wox.config.WoxConstant;
import com.shareyi.simpleserver.servlet.ApplicationContext;
import com.shareyi.simpleserver.servlet.FilterConfig;
import com.shareyi.simpleserver.servlet.WebApplication;

public class WoxConfiguartion {
	
	/**
	 * the package bean store in namespace map
	 */
	private HashMap<String,List<PackageBean>> namespaceMap;

	/**
	 * package直接以name为key值存入map之中
	 */
	private HashMap<String, PackageBean> packageBeanMap;
	
	
	private HashMap<String, String> constantMap;

	private ObjectFactory objectFactory;
	
	
	
	public WoxConfiguartion(WoxConfig woxConfig, ApplicationContext applicationContext) throws WoxConfigException{
		parseWoxConfig(woxConfig,applicationContext);
	}
	
	
	public void parseWoxConfig(WoxConfig woxConfig, ApplicationContext applicationContext) throws  WoxConfigException{
		namespaceMap=new HashMap<String,List<PackageBean>>();
		packageBeanMap=new HashMap<String, PackageBean>();
		
		List<PackageBean> packageBeans=woxConfig.getPackageBeans();
		if(packageBeans!=null){
			
			/**
			 * 分解到命名空间map中，为之后的通过命名空间查询提供便利
			 * 分解到包名map中，为继承关系提供便利
			 */
			for (PackageBean packageBean : packageBeans) {
				//TODO 简化校验    放入包名package中
				String packageName=packageBean.getName();
				if(packageName==null||packageName.trim().length()==0){
					throw new WoxConfigException("package name can't be null");
				}
				if(packageBeanMap.containsKey(packageName)){
					throw new WoxConfigException("duplicate define package ,the package name is ["+packageName+"]");
				}
				
				packageBeanMap.put(packageName,packageBean);
				
				//放入命名空间map之中
				List<PackageBean> packageBeanList=namespaceMap.get(packageBean.getNamespace());
				if(packageBeanList==null){
					packageBeanList=new ArrayList<PackageBean>();
					namespaceMap.put(packageBean.getNamespace(), packageBeanList);
				}
				packageBeanList.add(packageBean);
				
			}
			
			
			List<PackageBean> topPackageBeans=new ArrayList<PackageBean>();
			/**
			 * 解析继承关系
			 */
			for (PackageBean packageBean : packageBeans) {
				String extendName=packageBean.getExtends();
				if(StringUtils.isNotEmpty(extendName)){
					PackageBean parentPackageBean=packageBeanMap.get(extendName);
					if(parentPackageBean==null){
						throw new WoxConfigException("the package ["+packageBean.getName()+"]'s extends package ["+extendName+"] does't exist!");
					}
					
					packageBean.setParentPackageBean(parentPackageBean);
					if(parentPackageBean.checkCircleExtends(packageBean.getName())){
						throw new WoxConfigException("the package ["+packageBean.getName()+"] has circle extends ,please check it !");
					}
				}else{  //顶层包
					topPackageBeans.add(packageBean);
				}
			}
			
			
			//分析验证拦截器选项
			for (PackageBean packageBean : topPackageBeans) {
				packageBean.analyseInterceptorStack();
			}
		}
		this.constantMap=woxConfig.getConstantMap();
		
		
		
		String obectFactoryName=this.getConstantValue(WoxConstant.WOX_ObjectFactory);
		if(WoxConstant.WOX_ObjectFactory_Spring.equals(obectFactoryName)){
			this.objectFactory=(ObjectFactory) applicationContext.getParamValue(WoxConstant.WOX_ObjectFactory);
		}

		if(objectFactory==null){
			this.objectFactory=new ObjectFactory();
		}
	}

	
	
	
	
	
	
	public HashMap<String, List<PackageBean>> getNamespaceMap() {
		return namespaceMap;
	}



	public HashMap<String, PackageBean> getPackageBeanMap() {
		return packageBeanMap;
	}


	public HashMap<String, String> getConstantMap() {
		return constantMap;
	}

	
	public String getConstantValue(String key){
		return constantMap.get(key);
	}

	public ObjectFactory getObjectFactory() {
		return objectFactory;
	}

	public void setObjectFactory(ObjectFactory objectFactory) {
		this.objectFactory = objectFactory;
	}
	
}
