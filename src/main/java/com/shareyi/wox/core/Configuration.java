package com.shareyi.wox.core;

import java.util.List;
import java.util.Map;

import com.shareyi.wox.config.ActionBean;
import com.shareyi.wox.config.InterceptorRef;
import com.shareyi.wox.config.PackageBean;

public interface Configuration {

	String getMethodName();

	boolean isAllowedMethod(String method);

	ActionProxy getActionProxy();

	List<InterceptorRef> getInterceptors();


	public String getNamespace();
	public void setNamespace(String namespace);
	public String getActionName();
	public void setActionName(String actionName);
	public ActionBean getActionBean();
	public void setActionBean(ActionBean actionBean);
	public PackageBean getPackageBean();
	public void setPackageBean(PackageBean packageBean);
	public void setMethodName(String methodName);
	public void setPatternResult(Map<String, String> patternResult);
	public Map<String, String> getPatternResult();
	public void setActionProxy(ActionProxy actionProxy);
	
	
	
}
