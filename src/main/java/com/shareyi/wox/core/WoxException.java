package com.shareyi.wox.core;

public class WoxException extends Exception {

	private static final long serialVersionUID = 7277050506933908387L;

	
	public WoxException(String message){
		super(message);
	}
	
	public WoxException(String message,Throwable t){
		super(message, t);
	}
	
}
