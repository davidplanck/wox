package com.shareyi.wox.config;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.shareyi.wox.core.interceptor.Interceptor;

public class InterceptorRef {

	private String name;
	private HashMap<String, String> paramMap=new HashMap<String, String>();
	
	private Integer refType;
	private InterceptorBean interceptorBean;
	private InterceptorStackBean interceptorStackBean;
	private Interceptor interceptor;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRefType() {
		return refType;
	}

	public void setRefType(Integer refType) {
		this.refType = refType;
	}
	
	
	
	
	public HashMap<String, String> getParamMap() {
		return paramMap;
	}

	public Map<String, String> getConfigParams() {
		Map<String,String> configParams=new HashMap<String, String>();
		configParams.putAll(interceptorBean.getParams());
		 configParams.putAll(paramMap);
		return configParams;
	}
	
	public void addConfigParam(ConfigParam configParam){
		paramMap.put(configParam.getParamName(), configParam.getParamValue());
	}
	
	public void addConfigParam(String key,String value){
		paramMap.put(key, value);
	}


	public void setInterceptorBean(InterceptorBean interceptorBean) {
		this.interceptorBean=interceptorBean;
		
	}

	public InterceptorStackBean getInterceptorStackBean() {
		return interceptorStackBean;
	}

	public void setInterceptorStackBean(InterceptorStackBean interceptorStackBean) {
		this.interceptorStackBean = interceptorStackBean;
	}

	public InterceptorBean getInterceptorBean() {
		return interceptorBean;
	}

	public String getClassName() {
		if(this.refType==WoxConstant.INTERCEPTOR_REF_TYPE_BEAN){
			return interceptorBean.getClassName();
		}
		return null;
	}

	public Interceptor getInterceptor() {
		return this.interceptor;
	}

	public void setInterceptor(Interceptor interceptor2) {
		this.interceptor=interceptor2;
	}
	
	/**
	 * 如果是简单的拦截器指向
	 * @return
	 */
	public boolean isSimpleBeanRef(){
		return (this.refType==WoxConstant.INTERCEPTOR_REF_TYPE_BEAN);
	}

	/**
	 * 如果是堆载
	 * @return
	 */
	public boolean isStackRef(){
		return (this.refType==WoxConstant.INTERCEPTOR_REF_TYPE_STACK);
	}
	
	
	
	/**
	 * 简化为单一的bean指向
	 * @return
	 */
	public List<InterceptorRef> simplifyInterceptorRefStack() {
		List<InterceptorRef> simpleRefs=new LinkedList<InterceptorRef>();
		
		if(!isStackRef()){
			List<InterceptorRef> interceptorRefs=interceptorStackBean.getInterceptorRefs();
			if(interceptorRefs!=null&&!interceptorRefs.isEmpty()){
				for (InterceptorRef interceptorRef : interceptorRefs) {
						if(interceptorRef.isStackRef()){
							simpleRefs.addAll(interceptorRef.simplifyInterceptorRefStack());
						}else{
							simpleRefs.add(interceptorRef);
						}
				}
			}
		}
		
		return simpleRefs;
		
	}
	
	
}
