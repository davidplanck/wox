package com.shareyi.wox.config;

import java.util.ArrayList;
import java.util.List;

import com.shareyi.wox.core.interceptor.Interceptor;
public class InterceptorStackBean {

	private String name;
	
	private List<InterceptorRef> interceptorRefs=new ArrayList<InterceptorRef>();
	private List<Interceptor> interceptorList;
	
	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public List<InterceptorRef> getInterceptorRefs() {
		return interceptorRefs;
	}




	public void addInterceptorRef(InterceptorRef interceptorRef){
		interceptorRefs.add(interceptorRef);
	}


	
}
