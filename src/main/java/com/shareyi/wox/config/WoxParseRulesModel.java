package com.shareyi.wox.config;


import java.io.File;

import org.apache.commons.digester3.xmlrules.FromXmlRulesModule;

import com.shareyi.fileutil.FileUtil;


public class WoxParseRulesModel   extends FromXmlRulesModule  {

	@Override
	protected void loadRules() {
		File file=new File(FileUtil.getRuntimeFilePath( "/config/sys/wox-rules.xml" ));
		 loadXMLRules(file );
		
	}
}
