package com.shareyi.wox.config;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class PackageBean {


	//字段定义
	    private String name;	//packagename
	    private String namespace;	//namespace
	    private String extend;	//extend the package 
	    
	    private List<ActionBean> actionBeans=new ArrayList<ActionBean>(); 
	    private InterceptorsBean interceptorsBean; 
	    private InterceptorRef defaultInterceptorRef;
	    
	    
	    private PackageBean parentPackageBean;
	    private List<PackageBean> childsPackages;
	    
	    private List<ResultTypeBean> resultTypeBeans;
	    private List<ResultBean> globalResultBeans;
	    
	//getter and setter
	    public void setName(String name)
	    {
	     this.name=name;
	    }

	    public String getName()
	    {
	     return this.name;
	    }
	    public void setNamespace(String namespace)
	    {
	     this.namespace=namespace;
	    }

	    public String getNamespace()
	    {
		    if(namespace==null){
		    	namespace="";
		    }
		     return this.namespace;
	    }
	    
	    public void setExtends(String extend)
	    {
	     this.extend=extend;
	    }

	    public String getExtends()
	    {
	     return this.extend;
	    }
	    
	    

		public List<ActionBean> getActionBeans() {
			return actionBeans;
		}

		public void addActionBean(ActionBean actionBean) {
			this.actionBeans.add(actionBean);
			actionBean.setPackageBean(this);
		}

		public InterceptorsBean getInterceptorsBean() {
			return interceptorsBean;
		}

		public void setInterceptorsBean(InterceptorsBean interceptorsBean) {
			this.interceptorsBean = interceptorsBean;
		}


		public InterceptorRef getDefaultInterceptorRef() {
			return defaultInterceptorRef;
		}

		public void setDefaultInterceptorRef(InterceptorRef defaultInterceptorRef) {
			this.defaultInterceptorRef = defaultInterceptorRef;
		}

		public PackageBean getParentPackageBean() {
			return parentPackageBean;
		}

		public void setParentPackageBean(PackageBean parentPackageBean) {
			this.parentPackageBean = parentPackageBean;
			parentPackageBean.addChildsBean(this);
		}

		
		public List<PackageBean> getChildsPackages() {
			return childsPackages;
		}

		
		
		public void addChildsBean(PackageBean packageBean){
			if(childsPackages==null){
				childsPackages=new ArrayList<PackageBean>();
			}
			childsPackages.add(packageBean);
		}
		
		

		
		
		public List<ResultTypeBean> getResultTypeBeans() {
			return resultTypeBeans;
		}

		public void setResultTypeBeans(List<ResultTypeBean> resultTypeBeans) {
			this.resultTypeBeans = resultTypeBeans;
		}

		public List<ResultBean> getGlobalResultBeans() {
			return globalResultBeans;
		}

		public void setGlobalResultBeans(List<ResultBean> globalResultBeans) {
			this.globalResultBeans = globalResultBeans;
		}

		/**
		 * 检查是否含有闭合继承
		 * @param name2
		 */
		public boolean checkCircleExtends(String packageName) {
			if(extend!=null){
				if(extend.equals(packageName) || name.equals(packageName)){
					return true;
				} 
				
				if(parentPackageBean!=null){
					return parentPackageBean.checkCircleExtends(packageName);
				}
				
			}
			return false;
		}


		
		/**
		 * 解析拦截器堆载
		 * @throws WoxConfigException 
		 */
		public void analyseInterceptorStack() throws WoxConfigException {
			
			//检查拦截器定义是否合法
			if(interceptorsBean!=null){
				interceptorsBean.analyse(this);
			}
			
			
			//检查子包的拦截器定义是否合法
			if (childsPackages!=null&&!childsPackages.isEmpty()) {
				for (PackageBean child : childsPackages) {
					child.analyseInterceptorStack();
				}
			}
			
			//检查默认拦截器是否合法
			if(defaultInterceptorRef!=null){
				this.analyseInterceptorRef(defaultInterceptorRef);
			}
			
			//检查action中的拦截器是否合法
			if(actionBeans!=null&&!actionBeans.isEmpty()){
				for (ActionBean actionBean : actionBeans) {
					List<InterceptorRef> actionInterceptorRefs=actionBean.getInterceptorRefs();
					if(actionInterceptorRefs!=null){
						for (InterceptorRef interceptorRef : actionInterceptorRefs) {
							this.analyseInterceptorRef(interceptorRef);
						}
					}
						
				}
			}
			
		}

		
		/**
		 * 检查是否存在,并解析之
		 * @param interceptorRef
		 * @throws WoxConfigException 
		 */
		private void analyseInterceptorRef(
				InterceptorRef interceptorRef) throws WoxConfigException {
			InterceptorRef interceptorRef2=getInterceptorRefByName(interceptorRef.getName());
			if(interceptorRef2==null){
				throw new WoxConfigException("the interceptor ref defined in package["+this.name+"] can't find!");
			}
			interceptorRef.setInterceptorBean(interceptorRef2.getInterceptorBean());
			interceptorRef.setInterceptorStackBean(interceptorRef2.getInterceptorStackBean());
			interceptorRef.setRefType(interceptorRef2.getRefType());
		}

		/**
		 * 从父包中获取拦截器定义ref
		 * @param refName
		 * @return
		 */
		public InterceptorRef getIntercetorRefFromParent(String refName) {
			if(parentPackageBean!=null){
				return parentPackageBean.getInterceptorRefByName(refName);
				
			}
			return null;
		}

		
		/**
		 * 获取本包及父包中该refName的拦截器指向
		 * @param refName
		 * @return 
		 */
		private InterceptorRef getInterceptorRefByName(String refName) {
			if(interceptorsBean!=null){
				InterceptorRef ref=interceptorsBean.getInterceptorRefByName(refName);
				if(ref!=null){
					return ref;
				}
			}
			
			return getIntercetorRefFromParent(refName);
				
		}
		
		
		

	    /*
	     * 获取默认的拦截器载名称，如果本包没有，取父包，父包也没用，返空值
	     * */
		public InterceptorRef getPackageDefaultInterceptorRef() {
			if(this.defaultInterceptorRef==null){
				if(parentPackageBean!=null){
					return parentPackageBean.getPackageDefaultInterceptorRef();
				}
			}
			
			return defaultInterceptorRef;
		}

		
		/**
		 * 简化拦截器，变成简单的志向bean
		 * @param interceptorRefs
		 * @return
		 */
		public List<InterceptorRef> getInterceptorRefsWithoutStack(
				List<InterceptorRef> interceptorRefs) {
			List<InterceptorRef> simpleRefs=new LinkedList<InterceptorRef>();
			for (InterceptorRef interceptorRef : interceptorRefs) {
				if(interceptorRef.isSimpleBeanRef()){
					simpleRefs.add(interceptorRef);
				}else{
					List<InterceptorRef> refs=interceptorRef.simplifyInterceptorRefStack();
					simpleRefs.addAll(refs);
				}
			}
			return simpleRefs;
		}

		/**
		 * 根据resultCode查询公共location
		 * @param resultCode
		 * @return
		 */
		public ResultBean findGlobalResultByResultCode(String resultCode) {
			if(globalResultBeans!=null){
				for (ResultBean	 resultBean: globalResultBeans) {
					if(resultCode.equals(resultBean.getName())){
						return resultBean;
					}
				}
			}
			
			if(parentPackageBean!=null){
				return parentPackageBean.findGlobalResultByResultCode(resultCode);
			}
			return null;
		}

		public ResultTypeBean getResultTypeByName(String typeName) {
			if(this.resultTypeBeans!=null){
				for (ResultTypeBean resultTypeBean : resultTypeBeans) {
					if(StringUtils.isEmpty(typeName)&&resultTypeBean.isDefaultAlias()){
						return resultTypeBean;
					}else if(resultTypeBean.getName().equals(typeName)){
						return  resultTypeBean;
					}
				}
			}
			
			
			if(parentPackageBean!=null){
				return parentPackageBean.getResultTypeByName(typeName);
			}
			
			return null;
		}
}
