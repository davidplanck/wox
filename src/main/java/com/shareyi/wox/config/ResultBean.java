package com.shareyi.wox.config;

public class ResultBean {



	//字段定义
	    private String name;
	    private String type;
	    private String path;

	//getter and setter
	    public void setName(String name)
	    {
	     this.name=name;
	    }

	    public String getName()
	    {
	     return this.name;
	    }
	    
	    public void setType(String type)
	    {
	     this.type=type;
	    }

	    public String getType()
	    {
	     return this.type;
	    }
	    
	    public void setPath(String path)
	    {
	     this.path=path;
	    }

	    public String getPath()
	    {
	     return this.path;
	    }

}
