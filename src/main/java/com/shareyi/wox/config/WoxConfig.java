package com.shareyi.wox.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * wox configurations bean
 * @author zhangshibin
 *
 */
public class WoxConfig {

	private List<PackageBean> packageBeans=new ArrayList<PackageBean>();

	private HashMap<String, String> constantMap=new HashMap<String, String>();

	private List<String> includeFiles=new ArrayList<String>();

	public List<PackageBean> getPackageBeans() {
		return packageBeans;
	}

	public void setPackageBeans(List<PackageBean> packageBeans) {
		this.packageBeans = packageBeans;
	}

	public HashMap<String, String> getConstantMap() {
		return constantMap;
	}



	public List<String> getIncludeFiles() {
		return includeFiles;
	}

	public void setIncludeFiles(List<String> includeFiles) {
		this.includeFiles = includeFiles;
	}



	/**
	 * 合并config文件
	 * @param woxConfig2
	 */
	public void merge(WoxConfig woxConfig2) {
		if(woxConfig2!=null){
			packageBeans.addAll(woxConfig2.getPackageBeans());
			constantMap.putAll(woxConfig2.getConstantMap());
			includeFiles.addAll(woxConfig2.getIncludeFiles());
		}
		
	}

	public void addConstantParam(ConstantParamBean constantParamBean) {
		constantMap.put(constantParamBean.getName(),constantParamBean.getValue());
	}
	
	
	/**
	 * add include file
	 */

	public void addIncludeFile(IncludeFileBean bean){
		includeFiles.add(bean.getFile());
	}
	
	public void addPackageBean(PackageBean packageBean){
		packageBeans.add(packageBean);
	}
}
