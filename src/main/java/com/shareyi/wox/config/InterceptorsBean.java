package com.shareyi.wox.config;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InterceptorsBean {
	
	private Map<String,InterceptorBean> interceptorBeanMap=new HashMap<String,InterceptorBean>();
	/* 解析的时候放入*/
	private Map<String, InterceptorStackBean> interceptorStackMap=new HashMap<String, InterceptorStackBean>();
	private List<InterceptorStackBean> interceptorStackBeans=new LinkedList<InterceptorStackBean>();
	
	
	public void addInterceptorBean(InterceptorBean interceptorBean){
		this.interceptorBeanMap.put(interceptorBean.getName(),interceptorBean);
	}
	
	public void addInterceptorStackBean(InterceptorStackBean interceptorStackBean){
		this.interceptorStackBeans.add(interceptorStackBean);
		interceptorStackMap.put(interceptorStackBean.getName(), interceptorStackBean);
	}
	


	public Map<String, InterceptorBean> getInterceptorBeanMap() {
		return interceptorBeanMap;
	}

	public List<InterceptorStackBean> getInterceptorStackBeans() {
		return interceptorStackBeans;
	}

	
	/**
	 * 解析拦截器定义
	 * @param packageBean
	 * @throws WoxConfigException 
	 */
	public void analyse(PackageBean ownerPackageBean) throws WoxConfigException {
		if(interceptorStackBeans.size()!=0){
			for (InterceptorStackBean stackBean : interceptorStackBeans) {
				List<InterceptorRef> refs=stackBean.getInterceptorRefs();
				if(!refs.isEmpty()){
					for (InterceptorRef interceptorRef : refs) {
						String refName=interceptorRef.getName();
						
						//如果是定义的拦截器 bean
						InterceptorBean interceptorBean=interceptorBeanMap.get(refName);
						if(interceptorBean!=null){
							interceptorRef.setRefType(WoxConstant.INTERCEPTOR_REF_TYPE_BEAN);
							interceptorRef.setInterceptorBean(interceptorBean);
						}else{ //如果是定义的拦截器stack
							InterceptorStackBean interceptorStackBean=interceptorStackMap.get(refName);
							if(interceptorStackBean!=null){
								interceptorRef.setRefType(WoxConstant.INTERCEPTOR_REF_TYPE_STACK);
								interceptorRef.setInterceptorStackBean(interceptorStackBean);
							}else{  //如果本拦截器定义没有找到，委托packageBean去父包中查询
								InterceptorRef ref2=ownerPackageBean.getIntercetorRefFromParent(refName);
								if(ref2==null){
									throw new WoxConfigException("the interceptor ref ["+refName+"] defined in package ["+ownerPackageBean.getName()+"] is not defined!");
								}
							}
							
						}
						
						 
						
						
					}
					
				}
			}
			
		}
		
	}
	
	
	/**
	 * 根据拦截器，或者拦截器堆载的名称获取拦截器指向   只从本定义中查询
	 * @param refName
	 * @return
	 */

	public InterceptorRef getInterceptorRefByName(String refName) {
		//如果是定义的拦截器 bean
		InterceptorBean interceptorBean=interceptorBeanMap.get(refName);
		InterceptorRef interceptorRef=new InterceptorRef();
		interceptorRef.setName(refName);
		if(interceptorBean!=null){
			interceptorRef.setRefType(WoxConstant.INTERCEPTOR_REF_TYPE_BEAN);
			interceptorRef.setInterceptorBean(interceptorBean);
			return interceptorRef;
		}else{ //如果是定义的拦截器stack
			InterceptorStackBean interceptorStackBean=interceptorStackMap.get(refName);
			if(interceptorStackBean!=null){
				interceptorRef.setRefType(WoxConstant.INTERCEPTOR_REF_TYPE_STACK);
				interceptorRef.setInterceptorStackBean(interceptorStackBean);
				return interceptorRef;
			}
			
		}
		
		return null;
	}

	
	
}
