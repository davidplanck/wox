package com.shareyi.wox.config;

public class WoxConstant {

	/** 拦截器bean	 */
	public final static int INTERCEPTOR_REF_TYPE_BEAN=1;
	
	/** 拦截器堆载 	*/
	public final static int INTERCEPTOR_REF_TYPE_STACK=2;
	
	
	
	/** OBJECT fACTORY constant	 */
	public final static String WOX_ObjectFactory="wox.objectFactory";
	/** spring的自动封装	*/
	public final static String WOX_SpringAutoWire="wox.objectFactory.spring.autoWire";
	/** wox拦截的action后缀名	*/
	public final static String WOX_ActionExtension="wox.action.extension";
	
	public final static String WOX_ObjectFactory_Spring="spring";
	
}
