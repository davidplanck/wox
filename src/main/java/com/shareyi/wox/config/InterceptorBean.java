package com.shareyi.wox.config;

import java.util.HashMap;
import java.util.Map;

public class InterceptorBean {

	private String name;
	private String className;
	private Map<String,String> params=new HashMap<String, String>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	public void setClassName(String className) {
		this.className = className;
	}
	public void addParam(ConfigParam configParam){
		params.put(configParam.getParamName(), configParam.getParamValue());
	}
	
	public void addConfigParam(String key,String value){
		params.put(key, value);
	}

	public Map<String, String> getParams() {
		return params;
	}
	public String getClassName() {
		return this.className;
	}
	
	
}
