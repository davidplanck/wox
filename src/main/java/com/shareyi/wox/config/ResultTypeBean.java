package com.shareyi.wox.config;

import java.util.HashMap;
import java.util.Map;

public class ResultTypeBean {

	private String name;
	private String className;
	private boolean defaultAlias;
	private Map<String, Object> params;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public boolean isDefaultAlias() {
		return defaultAlias;
	}
	public void setDefaultAlias(boolean defaultAlias) {
		this.defaultAlias = defaultAlias;
	}

	public void addConfigParam(ConfigParam param) {
		if(params==null){
			params=new HashMap<String, Object>();
		}
		params.put(param.getParamName(),param.getParamValue());
	}
	
	public Map<String, Object> getParams() {
		
		return this.params;
	}

}
