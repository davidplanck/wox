package com.shareyi.wox.config;

public class WoxConfigException extends Exception {

	private static final long serialVersionUID = 7277050506933908387L;

	
	public WoxConfigException(String message){
		super(message);
	}
	
	public WoxConfigException(String message,Throwable t){
		super(message, t);
	}
	
}
