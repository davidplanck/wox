package com.shareyi.wox.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.shareyi.wox.core.ActionPatternHelper;
import com.shareyi.wox.util.MatchHelper;
import com.shareyi.wox.util.WildcardHelper;

public class ActionBean {



	//字段定义
	    private String name;
	    private String method;
	    private String className;
	    private List<InterceptorRef> interceptorRefs;
	    private PackageBean packageBean;
	    /* result list*/
	    private List<ResultBean> resultBeans=new ArrayList<ResultBean>();
	    private ActionPatternHelper actionPatternHelper;
	     
	//getter and setter
	    public void setName(String name)
	    {
	     this.name=name;
	    }

	    public String getName()
	    {
	     return this.name;
	    }
	    
	    public void setMethod(String method)
	    {
	     this.method=method;
	    }

	    public String getMethod()
	    {
	     return this.method;
	    }
	    

	    
	    public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		public List<InterceptorRef> getInterceptorRefs() {
			return interceptorRefs;
		}

		public PackageBean getPackageBean() {
			return packageBean;
		}

		public void setPackageBean(PackageBean packageBean) {
			this.packageBean = packageBean;
		}

		public void addInterceptorRef(InterceptorRef interceptorRef) {
			if(interceptorRefs==null){
				interceptorRefs=new ArrayList<InterceptorRef>();
			}
			
			interceptorRefs.add(interceptorRef);
		}

		public void addResultBean(ResultBean resultBean){
	    	resultBeans.add(resultBean);
	    }

		public ActionPatternHelper getActionPatternHelper() {
			return actionPatternHelper;
		}

		public void setActionPatternHelper(ActionPatternHelper actionPatternHelper) {
			this.actionPatternHelper = actionPatternHelper;
		}

		public Map<String, String> isMatchActionName(String actionName, WildcardHelper wildcardHelper) {
			Map<String,String> resultMap=new HashMap<String, String>();
			
			if(actionPatternHelper==null){
				this.actionPatternHelper=new ActionPatternHelper();
				actionPatternHelper.parseActionName(this.getName(),wildcardHelper);
			}
			if(actionPatternHelper.isLiteral()){
			  if(actionName.equals(this.name)){
				  resultMap.put("0", actionName);
				  return resultMap;
			  }
			}else{
				wildcardHelper.match(resultMap, actionName, actionPatternHelper.getPatternInfo());
				if (actionPatternHelper.isResultMapMatch(resultMap)) {
					return resultMap;
				}
			}
			
			return null;
		}

		public ResultBean getResultBeanByCodeAndPatternMap(String resultCode,
				Map<String, String> patternResult) {
			ResultBean resultBean=null;
			if(resultBeans!=null){
				for (ResultBean rb : resultBeans) {
					String resultName=rb.getName();
					if(StringUtils.isEmpty(resultName)){
						resultBean=rb;
						break;
					}else{
						resultName=MatchHelper.methodNameMatcher(resultName, patternResult);
						if(resultCode.equals(resultName)){
							resultBean=rb;
							break;
						}
					}
				}
			}
				if(resultBean==null){
					resultBean=this.packageBean.findGlobalResultByResultCode(resultCode);
					
					
					
				}
			
			return resultBean;
		}


		
}
