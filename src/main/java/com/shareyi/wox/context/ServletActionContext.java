package com.shareyi.wox.context;

import com.shareyi.simpleserver.core.HttpRequest;
import com.shareyi.simpleserver.core.HttpResponse;




public class ServletActionContext {
   
	
	public static HttpRequest getRequest() {
		return ActionContext.getContext().getRequest();
	}
	
	public static HttpResponse getResponse() {
		return ActionContext.getContext().getResponse();
	}

	public static String getActionName() {
		return ActionContext.getContext().getName();
	}

	public static void setActionContext(ActionContext actionContext) {
		ActionContext.setContext(actionContext);
		
	}

	public static ValueContext getValueContext(){
		return ActionContext.getContext().getValueContext();
	}

}
