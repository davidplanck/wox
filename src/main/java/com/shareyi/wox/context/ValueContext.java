package com.shareyi.wox.context;

import java.util.Map;

public interface ValueContext {

	  public abstract Map<String, Object> getContext();
	  
	  public abstract void setValue(String expr, Object value);

     void setParameter(String expr, String value);

     public abstract Object findValue(String expr);

     public abstract int size();
     
     public void putAll(Map map);


}
